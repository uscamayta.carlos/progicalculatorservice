# ProgiCalculator

## Overview

ProgiCalculator is an application designed that will allow a buyer to calculate the total price of a vehicle at a car auction, this application is using clean architecture. The solution is organized into multiple projects and layers, each with specific responsibilities to ensure a clear separation of concerns and maintain system flexibility and maintainability.

## Technologies Used

- .NET Core 8
- FluentValidation
- AutoMapper
- Entity Framework Core
- ASP.NET Core
- Swagger for API documentation
- xUnit for testing
- Moq for mocking in tests

## Project Structure

### Projects and Layers

1. **ProgiCalculator API** (`progicalculator/api`)
   - **Controllers**: Handle HTTP requests and return the appropriate responses.
   - **Models**: Define the data structures that are sent and received through the API.
2. **ProgiCalculator Common** (`progicalculatorCommon`)
   - **DTOs**: Data transfer objects used to communicate data between layers.
   - **Enums**: Enumeration types used throughout the application.
   - **Exceptions**: Handling and definition of common exceptions.
3. **ProgiCalculator Application** (`Core/ProgiCalculatorApplication`)
   - **Handlers**: Handle the execution of commands and queries.
   - **Mappers**: Map data between different layers using AutoMapper.
   - **Models**: Application-specific models.
   - **Queries**: Define and handle queries to retrieve data.
   - **Validators**: Validate data using FluentValidation.
4. **ProgiCalculator Core** (`Core/ProgiCalculatorCore`)
   - **Repositories Interfaces**: Define the contracts for data access that the infrastructure layer will implement.
5. **ProgiCalculator Domain** (`ProgiCalculatorDomain`)
   - **Database Models**: Classes representing the main business data.
   - **Configurations**: Database configurations for the models.
6. **ProgiCalculator Infrastructure** (`ProgiCalculatorInfraestructure`)
   - **DbContext**: The context for Entity Framework Core.
   - **Repositories Implementation**: Implementations of the repository interfaces defined in the core.
   - **Seeders**: Seed data for the database.
7. **ProgiCalculator Test** (`ProgiCalculatortest`)
   - **Controllers Tests**: Tests for API controllers.
   - **Repositories Tests**: Tests for repository implementations.

### Layer Descriptions

#### ProgiCalculator API

- **Purpose**: Expose the endpoints necessary to interact with the application from external clients.
- **Description**: Translates HTTP requests into domain actions and orchestrates interaction with the application and domain layers.
- **Structure**:
  - **Controllers**: Handle HTTP requests and return the appropriate responses.
  - **Models**: Define the data structures that are sent and received through the API.

#### ProgiCalculator Common

- **Purpose**: Contains common components and utilities used by other projects within the solution.
- **Description**: Provides cross-cutting functionalities like data transfer objects, enums, and exceptions.
- **Structure**:
  - **DTOs**: Data transfer objects used to communicate data between layers.
  - **Enums**: Enumeration types used throughout the application.
  - **Exceptions**: Handling and definition of common exceptions.

#### ProgiCalculator Application

- **Purpose**: Contains the application logic, including use cases and operation orchestration.
- **Description**: Coordinates activities between the domain and infrastructure layers, implementing the system's use cases.
- **Structure**:
  - **Handlers**: Handle the execution of commands and queries.
  - **Mappers**: Map data between different layers using AutoMapper.
  - **Models**: Application-specific models.
  - **Queries**: Define and handle queries to retrieve data.
  - **Validators**: Validate data using FluentValidation.

#### ProgiCalculator Core

- **Purpose**: Contains domain entities and core business logic.
- **Description**: Encapsulates fundamental business rules and entities that represent key concepts of the system.
- **Structure**:
  - **Repositories Interfaces**: Define the contracts for data access that the infrastructure layer will implement.

#### ProgiCalculator Domain

- **Purpose**: Defines core entities and business logic.
- **Description**: Contains entities, aggregates, domain services, and business logic independent of other layers.
- **Structure**:
  - **Database Models**: Classes representing the main business data.
  - **Configurations**: Database configurations for the models.

#### ProgiCalculator Infrastructure

- **Purpose**: Implements interfaces defined in the domain layer for data access and other external services.
- **Description**: Contains implementations of persistence, integration with other systems, and external services.
- **Structure**:
  - **DbContext**: The context for Entity Framework Core.
  - **Repositories Implementation**: Implementations of the repository interfaces defined in the core.
  - **Seeders**: Seed data for the database.

#### ProgiCalculator Test

- **Purpose**: Contains unit and integration tests for the application.
- **Description**: Ensures that each system component works correctly in isolation (unit tests) and together (integration tests).
- **Structure**:
  - **Controllers Tests**: Tests for API controllers.
  - **Repositories Tests**: Tests for repository implementations.

## Installation and Execution

### Prerequisites

- .NET Core 8 SDK
-  Visual Studio 2022

### Steps

1. Clone the repository.
2. Restore NuGet packages:
    ```bash
    dotnet restore
    ```
3. Configure the connection string in the `appsettings.json` file.
4. Databse used:
    ```bash
    this solution is using database in memory
    ```
5. Run the application:
    ```bash
    dotnet run --project progicalculator/api
    ```

### Running Tests

To run unit and integration tests:

```bash
dotnet test
